import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.1.1"
    id("io.spring.dependency-management") version "1.1.0"
    id("com.gorylenko.gradle-git-properties") version "2.4.1"
    kotlin("jvm") version "1.8.22"
    kotlin("plugin.spring") version "1.8.22"
    kotlin("plugin.jpa") version "1.8.22"
}

group = "tech.visdom"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

dependencies {

    // Spring
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-actuator")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-data-jpa")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-validation")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-web")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-security")
    annotationProcessor(group = "org.springframework.boot", name = "spring-boot-configuration-processor")

    // Kotlin
    implementation(group = "org.jetbrains.kotlin", name = "kotlin-reflect")
    implementation(group = "io.github.microutils", name = "kotlin-logging", version = "3.0.5")

    // Postgres
    runtimeOnly(group = "org.postgresql", name = "postgresql", version = "42.6.0")

    // Jackson
    implementation(group = "com.fasterxml.jackson.module", name = "jackson-module-kotlin", version = "2.15.2")

    // JWT
    implementation(group = "io.jsonwebtoken", name = "jjwt", version = "0.9.1")

    // Model mapper
    implementation(group = "org.modelmapper", name = "modelmapper", version = "3.1.1")

    // Liquibase
    implementation(group = "org.liquibase", name = "liquibase-core", version = "4.23.0")

    // Metrics
    runtimeOnly(group = "io.micrometer", name = "micrometer-registry-prometheus")

    // SpringDoc
    implementation(group = "org.springdoc", name = "springdoc-openapi-starter-webmvc-ui", version = "2.1.0")

    // Tests
    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-test")
    testImplementation(group = "org.springframework.security", name = "spring-security-test")
}


tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
