CREATE TABLE IF NOT EXISTS "user"
(
    "id"                              BIGSERIAL PRIMARY KEY,
    "first_name"                      VARCHAR(256) NOT NULL,
    "family_name"                     VARCHAR(256) NOT NULL,
    "username"                        VARCHAR(256) NOT NULL,
    "password"                        VARCHAR(256) NOT NULL,
    "security_stamp"                  VARCHAR(256) NOT NULL,
    "registration_date"               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "last_password_change_date"       TIMESTAMP,
    "last_password_reset_date"        TIMESTAMP,
    "account_locked"                  BOOLEAN NOT NULL DEFAULT FALSE,
    "password_temporary"              BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE UNIQUE INDEX IF NOT EXISTS user_username_unique_index
    on "user" ("username");

CREATE TABLE IF NOT EXISTS role
(
    "id"                              VARCHAR(64) PRIMARY KEY,
    "description"                     VARCHAR(256)
);

CREATE TABLE IF NOT EXISTS user_role
(
    "user_id"                         BIGINT references "user",
    "role_id"                         VARCHAR(64) references role
);

CREATE INDEX IF NOT EXISTS user_role_user_id_index
    on user_role ("user_id");
