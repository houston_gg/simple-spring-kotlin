package tech.visdom.springappexample.ulit

import java.security.SecureRandom

private const val DEFAULT_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!?@#$%^&*_-+="
private const val NUMERIC_CHARACTERS = "0123456789"
private const val DEFAULT_SECURITY_STAMP_LENGTH = 256

fun generateSecurityStamp(targetKeyLength: Int = DEFAULT_SECURITY_STAMP_LENGTH): String =
    generate(targetKeyLength, DEFAULT_CHARACTERS)

fun generatePassword(targetKeyLength: Int = 20): String =
    generate(targetKeyLength, DEFAULT_CHARACTERS)

fun generate(targetKeyLength: Int, characters: String): String {
    val random = SecureRandom()
    val buffer = StringBuilder(targetKeyLength)
    for (i in 0 until targetKeyLength) {
        buffer.append(characters[random.nextInt(characters.length)])
    }
    return buffer.toString()
}

