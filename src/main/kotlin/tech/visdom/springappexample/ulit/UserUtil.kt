package tech.visdom.springappexample.ulit

import tech.visdom.springappexample.dto.user.UpdateUserDto
import tech.visdom.springappexample.entity.user.UserEntity

fun UserEntity.update(update: UpdateUserDto) = apply {
    update.firstName?.let {
        if (it.isNotBlank()) {
            firstName = it
        }
    }
    update.familyName?.let {
        if (it.isNotBlank()) {
            familyName = it
        }
    }
}
