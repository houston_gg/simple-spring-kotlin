package tech.visdom.springappexample.ulit

import org.modelmapper.ModelMapper

inline fun <T, reified R> ModelMapper.map(obj: T): R = this.map(obj, R::class.java)
