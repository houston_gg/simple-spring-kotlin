package tech.visdom.springappexample.ulit.error

import org.springframework.http.HttpStatus

data class ResolvedErrorResponse(
    val message: String?,
    val status: HttpStatus,
    val statusCode: Int,
    val errorCode: String,
    val path: String
)
