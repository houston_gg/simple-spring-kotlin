package tech.visdom.springappexample.ulit.error

const val UNRESOLVED = "UNRESOLVED" // Unresolved error
const val UAE = "UAE" // User already exist
const val UNF = "UNF" // User not found
const val IUC = "IUC" // Invalid user credentials
const val UAL = "UAL" // User account locked
