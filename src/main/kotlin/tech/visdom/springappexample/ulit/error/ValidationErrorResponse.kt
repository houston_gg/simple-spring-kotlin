package tech.visdom.springappexample.ulit.error

import org.springframework.http.HttpStatus

data class ValidationErrorResponse(
    val message: String?,
    val validationErrors: List<String>,
    val status: HttpStatus,
    val statusCode: Int,
    val errorCode: String,
    val path: String
)
