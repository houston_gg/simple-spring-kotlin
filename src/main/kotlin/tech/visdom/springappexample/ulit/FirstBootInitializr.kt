package tech.visdom.springappexample.ulit

import jakarta.annotation.PostConstruct
import mu.KLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Configuration
import tech.visdom.springappexample.dto.user.CreateUserDto
import tech.visdom.springappexample.entity.user.RoleEntity
import tech.visdom.springappexample.service.user.RoleService
import tech.visdom.springappexample.service.user.UserService

@Configuration
@ConditionalOnProperty(name = ["first-boot-user-initialization.enabled"])
class FirstBootInitializr(
    private val userService: UserService,
    private val roleService: RoleService
) {

    @PostConstruct
    fun initializeAdminUserAndRoles() {
        if (!userService.anyUserExists()) {
            logger.info { "Initializing default admin user" }
            val roles = listOf(
                RoleEntity().apply {
                    id = ROLE_ADMINISTRATOR
                    description = "Service administrator role"
                },
                RoleEntity().apply {
                    id = ROLE_USER
                    description = "Regular user role"
                }
            )
            val password = generatePassword()
            val newUser = CreateUserDto(
                "SYSTEM",
                "ADMINISTRATOR",
                "administrator",
                password
            )
            roleService.createRoles(roles)
            userService.createUserEntity(newUser, roles.map { it.id })
            logger.info { "Created new admin user with username: [${newUser.username}] and password: [${password}]" }
        } else {
            logger.warn { "First boot initialization feature can be disabled, because database already contains users" }
        }
    }

    companion object : KLogging()
}
