package tech.visdom.springappexample.exception

import org.springframework.http.HttpStatus
import tech.visdom.springappexample.ulit.error.UAL

class AccountLockedException(
    override val message: String?,
    override val status: HttpStatus,
    override val errorCode: String
) : ResponseError(message, status, errorCode)

fun accountLockedError(message: String?, status: HttpStatus = HttpStatus.LOCKED, errorCode: String = UAL): Nothing =
    throw AccountLockedException(message, status, errorCode)
