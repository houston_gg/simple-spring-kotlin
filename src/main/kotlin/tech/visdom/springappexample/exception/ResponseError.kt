package tech.visdom.springappexample.exception

import org.springframework.http.HttpStatus

open class ResponseError(
    override val message: String?,
    open val status: HttpStatus,
    open val errorCode: String
) : RuntimeException(message)
