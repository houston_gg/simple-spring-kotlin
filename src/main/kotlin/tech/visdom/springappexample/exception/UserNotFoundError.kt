package tech.visdom.springappexample.exception

import org.springframework.http.HttpStatus
import tech.visdom.springappexample.ulit.error.UNF

class UserNotFoundError(
    override val message: String?,
    override val status: HttpStatus,
    override val errorCode: String
) : ResponseError(message, status, errorCode)

fun userNotFoundError(message: String?, status: HttpStatus = HttpStatus.NOT_FOUND, errorCode: String = UNF): Nothing =
    throw UserNotFoundError(message, status, errorCode)
