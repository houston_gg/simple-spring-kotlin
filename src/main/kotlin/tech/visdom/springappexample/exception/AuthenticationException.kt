package tech.visdom.springappexample.exception

import org.springframework.http.HttpStatus
import tech.visdom.springappexample.ulit.error.IUC

class AuthenticationException(
    override val message: String?,
    override val status: HttpStatus,
    override val errorCode: String
) : ResponseError(message, status, errorCode)

fun authenticationError(
    message: String? = "[UNAUTHORIZED] Invalid user credentials",
    status: HttpStatus = HttpStatus.UNAUTHORIZED,
    errorCode: String = IUC
): Nothing = throw AuthenticationException(message, status, errorCode)
