package tech.visdom.springappexample.exception

import org.springframework.http.HttpStatus
import tech.visdom.springappexample.ulit.error.UAE

class UserAlreadyExistException(
    override val message: String?,
    override val status: HttpStatus,
    override val errorCode: String
) : ResponseError(message, status, errorCode)

fun userAlreadyExistError(message: String?, status: HttpStatus = HttpStatus.CONFLICT, errorCode: String = UAE): Nothing =
    throw UserAlreadyExistException(message, status, errorCode)