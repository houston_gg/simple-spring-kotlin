package tech.visdom.springappexample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan("tech.visdom.springappexample.configuration.properties")
class SpringAppExampleApplication

fun main(args: Array<String>) {
    runApplication<SpringAppExampleApplication>(*args)
}
