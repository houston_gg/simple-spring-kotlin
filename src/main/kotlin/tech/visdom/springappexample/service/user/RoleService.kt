package tech.visdom.springappexample.service.user

import org.springframework.stereotype.Service
import tech.visdom.springappexample.dao.RoleDao
import tech.visdom.springappexample.entity.user.RoleEntity

@Service
class RoleService(
    private val roleDao: RoleDao
) {
    fun createRoles(roles: List<RoleEntity>) = roleDao.saveAll(roles)
}
