package tech.visdom.springappexample.service.user

import org.springframework.context.annotation.Lazy
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import tech.visdom.springappexample.configuration.jwt.JwtUserDetails
import tech.visdom.springappexample.entity.user.RoleEntity
import tech.visdom.springappexample.entity.user.UserEntity


@Service
class UserDetailsServiceImpl(
    @Lazy private val userService: UserService
): UserDetailsService {

    override fun loadUserByUsername(username: String): JwtUserDetails {
        val user: UserEntity = userService.findUserEntityByUsernameOrNull(username)
            ?: throw UsernameNotFoundException("User with username [$username] not found.")

        return JwtUserDetails(user, mapRolesToAuthorities(user.roles))
    }

    fun loadUserById(id: Long): JwtUserDetails {
        val user = userService.findUserEntityByIdOrNull(id)
            ?: throw UsernameNotFoundException("User with ID [$id] not found.")

        return JwtUserDetails(user, mapRolesToAuthorities(user.roles))
    }

    private fun mapRolesToAuthorities(roles: Collection<RoleEntity>): MutableCollection<GrantedAuthority> =
        roles.mapTo(mutableListOf()) {
            SimpleGrantedAuthority(it.id)
        }
}
