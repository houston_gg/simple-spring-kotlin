package tech.visdom.springappexample.service.user

import org.modelmapper.ModelMapper
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import tech.visdom.springappexample.dao.UserDao
import tech.visdom.springappexample.dto.user.CreateUserDto
import tech.visdom.springappexample.dto.user.UpdateUserDto
import tech.visdom.springappexample.dto.user.UserDto
import tech.visdom.springappexample.entity.user.RoleEntity
import tech.visdom.springappexample.entity.user.UserEntity
import tech.visdom.springappexample.exception.userAlreadyExistError
import tech.visdom.springappexample.exception.userNotFoundError
import tech.visdom.springappexample.ulit.generateSecurityStamp
import tech.visdom.springappexample.ulit.map
import tech.visdom.springappexample.ulit.update


@Service
class UserService(
    private val userDao: UserDao,
    private val passwordEncoder: PasswordEncoder,
    private val modelMapper: ModelMapper,
) {

    fun findUserEntityByIdOrNull(userId: Long): UserEntity? = userDao.findByIdOrNull(userId)

    fun findUserEntityById(userId: Long): UserEntity = findUserEntityByIdOrNull(userId)
        ?: userNotFoundError("User with ID: [$userId] not found")

    fun findUserDtoById(userId: Long): UserDto = modelMapper.map(findUserEntityById(userId))

    fun findUserEntityByUsernameOrNull(username: String): UserEntity? = userDao.findByUsername(username)

    fun findUserEntityByUsername(username: String): UserEntity = userDao.findByUsername(username)
        ?: userNotFoundError("User with username: [$username] not found")

    fun findUserDtoByUsername(username: String): UserDto = modelMapper.map(findUserEntityByUsername(username))

    fun getAuthorizedUserEntity(): UserEntity {
        val userDetails = SecurityContextHolder.getContext().authentication.principal as UserDetails
        return findUserEntityByUsername(userDetails.username)
    }

    fun getAuthorizedUserDto(): UserDto = modelMapper.map(getAuthorizedUserEntity())

    fun createUserEntity(newUser: CreateUserDto, userRoles: List<String>? = null): UserEntity {
        val user = modelMapper.map<CreateUserDto, UserEntity>(newUser).apply {
            password = passwordEncoder.encode(newUser.password)
            securityStamp = generateSecurityStamp()
            roles = userRoles?.map { RoleEntity().apply { id = it } }?.toHashSet() ?: getBaseUserRoles()
        }

        return try {
            userDao.save(user)
        } catch (e: DataIntegrityViolationException) {
            userAlreadyExistError("User with username: [${newUser.username}] already exists")
        }
    }

    fun createUserDto(newUser: CreateUserDto, userRoles: List<String>? = null): UserDto =
        modelMapper.map(createUserEntity(newUser, userRoles))

    fun updateUser(userId: Long, update: UpdateUserDto): UserDto {
        val user = findUserEntityById(userId)
        user.update(update)

        return modelMapper.map(userDao.save(user))
    }

    @Transactional
    fun deleteUserById(userId: Long) {
        userDao.deleteById(userId)
    }

    fun anyUserExists() = userDao.existsByIdIsGreaterThan(0L)

    private fun getBaseUserRoles() = mutableSetOf(RoleEntity().apply { id = "ROLE_USER" })
}
