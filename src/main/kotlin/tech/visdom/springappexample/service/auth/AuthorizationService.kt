package tech.visdom.springappexample.service.auth

import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import tech.visdom.springappexample.configuration.jwt.JwtProvider
import tech.visdom.springappexample.configuration.jwt.request.AuthRequest
import tech.visdom.springappexample.configuration.jwt.request.RefreshRequest
import tech.visdom.springappexample.configuration.jwt.token.TokenPair
import tech.visdom.springappexample.configuration.jwt.token.TokenType
import tech.visdom.springappexample.exception.accountLockedError
import tech.visdom.springappexample.exception.authenticationError
import tech.visdom.springappexample.service.user.UserService

@Service
class AuthorizationService(
    private val userService: UserService,
    private val jwtProvider: JwtProvider,
    private val passwordEncoder: PasswordEncoder,
) {

    fun authAndGenerateTokenPair(request: AuthRequest): TokenPair {
        val login = requireNotNull(request.login)
        val password = requireNotNull(request.password)
        val user = userService.findUserEntityByUsernameOrNull(login) ?: authenticationError()
        if (!passwordEncoder.matches(password, user.password)) {
            authenticationError()
        }
        if (user.accountLocked) {
            accountLockedError("Account with ID [" + user.id + "] is locked")
        }

        return jwtProvider.generateTokenPair(user)
    }

    fun refreshTokenPair(request: RefreshRequest): TokenPair {
        val refreshToken = requireNotNull(request.refreshToken)
        if (!jwtProvider.validateToken(refreshToken, TokenType.REFRESH_TOKEN)) {
            authenticationError()
        }
        val accessTokenClaims = jwtProvider.getTokenClaims(refreshToken, TokenType.REFRESH_TOKEN)
        val user = userService.findUserEntityByIdOrNull(accessTokenClaims.id) ?: authenticationError()
        if (accessTokenClaims.securityStamp != user.securityStamp) {
            authenticationError()
        }
        if (user.accountLocked) {
            accountLockedError("Account with ID [" + user.id + "] is locked")
        }

        return jwtProvider.generateTokenPair(user)
    }
}
