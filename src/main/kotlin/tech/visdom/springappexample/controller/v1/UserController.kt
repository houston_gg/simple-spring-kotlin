package tech.visdom.springappexample.controller.v1

import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tech.visdom.springappexample.dto.user.CreateUserDto
import tech.visdom.springappexample.dto.user.UpdateUserDto
import tech.visdom.springappexample.dto.user.UserDto
import tech.visdom.springappexample.service.user.UserService

@RestController
@RequestMapping("/api/v1/users")
class UserController(
    private val userService: UserService
) {

    @GetMapping("/user")
    fun getCurrentAuthorizedUser(): ResponseEntity<UserDto> = ResponseEntity.ok(userService.getAuthorizedUserDto())

    @GetMapping("/user/{user_id}")
    @Secured(value = ["ROLE_ADMINISTRATOR"])
    fun findUserById(@PathVariable(value = "user_id") userId: Long): ResponseEntity<UserDto> =
        ResponseEntity.ok(userService.findUserDtoById(userId))

    @PostMapping("/user")
    @Secured(value = ["ROLE_ADMINISTRATOR"])
    fun createUser(@Valid @RequestBody newUser: CreateUserDto): ResponseEntity<UserDto> =
        ResponseEntity.ok(userService.createUserDto(newUser))

    @PutMapping("/user/{user_id}")
    @Secured(value = ["ROLE_ADMINISTRATOR"])
    fun updateUser(
        @PathVariable(value = "user_id") userId: Long,
        @RequestBody update: UpdateUserDto
    ): ResponseEntity<UserDto> = ResponseEntity.ok(userService.updateUser(userId, update))

    @DeleteMapping("/user/{user_id}")
    @Secured(value = ["ROLE_ADMINISTRATOR"])
    fun deleteUserById(@PathVariable(value = "user_id") userId: Long) {
        userService.deleteUserById(userId)
    }
}
