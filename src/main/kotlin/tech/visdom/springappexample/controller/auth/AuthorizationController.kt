package tech.visdom.springappexample.controller.auth

import io.swagger.v3.oas.annotations.security.SecurityRequirements
import jakarta.validation.Valid
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tech.visdom.springappexample.configuration.jwt.request.AuthRequest
import tech.visdom.springappexample.configuration.jwt.request.RefreshRequest
import tech.visdom.springappexample.service.auth.AuthorizationService

@RestController
@RequestMapping("/auth")
@SecurityRequirements
class AuthorizationController(
    private val authorizationService: AuthorizationService
) {

    @PostMapping("/access")
    fun auth(@Valid @RequestBody request: AuthRequest) = authorizationService.authAndGenerateTokenPair(request)

    @PostMapping("/refresh")
    fun refresh(@Valid @RequestBody request: RefreshRequest) = authorizationService.refreshTokenPair(request)
}
