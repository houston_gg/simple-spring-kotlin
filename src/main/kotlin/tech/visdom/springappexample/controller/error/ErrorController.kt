package tech.visdom.springappexample.controller.error

import jakarta.servlet.http.HttpServletRequest
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import tech.visdom.springappexample.exception.ResponseError
import tech.visdom.springappexample.ulit.error.ResolvedErrorResponse
import tech.visdom.springappexample.ulit.error.UNRESOLVED
import tech.visdom.springappexample.ulit.error.ValidationErrorResponse


@RestControllerAdvice
class ErrorController {

    @ExceptionHandler
    fun responseErrorExceptionHandler(
        request: HttpServletRequest,
        exception: ResponseError
    ): ResponseEntity<ResolvedErrorResponse> {
        logger.debug("Resolved :[${exception.javaClass}] with status: [${exception.status}] and message: [${exception.message}] ")
        val response = ResolvedErrorResponse(
            message = exception.message,
            status = exception.status,
            statusCode = exception.status.value(),
            errorCode = exception.errorCode,
            path = request.contextPath + request.servletPath
        )
        return ResponseEntity(response, exception.status)
    }

    @ExceptionHandler
    fun accessDeniedExceptionHandler(
        request: HttpServletRequest,
        exception: AccessDeniedException
    ): ResponseEntity<ResolvedErrorResponse> {
        val response = ResolvedErrorResponse(
            message = exception.message,
            status = HttpStatus.FORBIDDEN,
            statusCode = HttpStatus.FORBIDDEN.value(),
            errorCode = HttpStatus.FORBIDDEN.reasonPhrase,
            path = request.contextPath + request.servletPath
        )
        return ResponseEntity(response, HttpStatus.FORBIDDEN)
    }

    @ExceptionHandler
    fun methodArgumentNotValidExceptionHandler(
        request: HttpServletRequest,
        exception: MethodArgumentNotValidException
    ): ResponseEntity<ValidationErrorResponse> {
        val validationErrorsList = exception.bindingResult.allErrors.mapNotNull { e ->
            "${(e as FieldError).field}: ${e.getDefaultMessage()}"
        }

        val response = ValidationErrorResponse(
            message = "Request body validation failed",
            validationErrors = validationErrorsList,
            status = HttpStatus.BAD_REQUEST,
            statusCode = HttpStatus.BAD_REQUEST.value(),
            errorCode = HttpStatus.BAD_REQUEST.reasonPhrase,
            path = request.contextPath + request.servletPath
        )
        return ResponseEntity(response, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler
    fun unexpectedExceptionHandler(
        request: HttpServletRequest,
        exception: Throwable
    ): ResponseEntity<ResolvedErrorResponse> {
        logger.error(exception) { exception.message }
        val response = ResolvedErrorResponse(
            message = exception.message,
            status = HttpStatus.INTERNAL_SERVER_ERROR,
            statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value(),
            errorCode = UNRESOLVED,
            path = request.contextPath + request.servletPath
        )
        return ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    companion object {
        private val logger = KotlinLogging.logger { }
    }
}
