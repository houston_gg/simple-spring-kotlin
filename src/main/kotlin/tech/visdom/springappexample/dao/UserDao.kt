package tech.visdom.springappexample.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import tech.visdom.springappexample.entity.user.UserEntity

@Repository
interface UserDao: JpaRepository<UserEntity, Long> {
    fun findByUsername(username: String): UserEntity?

    fun existsByIdIsGreaterThan(id: Long): Boolean
}
