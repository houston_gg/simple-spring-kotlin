package tech.visdom.springappexample.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import tech.visdom.springappexample.entity.user.RoleEntity

@Repository
interface RoleDao: JpaRepository<RoleEntity, String>
