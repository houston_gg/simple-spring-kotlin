package tech.visdom.springappexample.dto.user

import jakarta.validation.constraints.NotBlank

data class CreateUserDto(
    @field:NotBlank var firstName: String?,
    @field:NotBlank var familyName: String?,
    @field:NotBlank var username: String?,
    @field:NotBlank var password: String?,
)
