package tech.visdom.springappexample.dto.user

data class UpdateUserDto(
    var firstName: String? = null,
    var familyName: String? = null
)
