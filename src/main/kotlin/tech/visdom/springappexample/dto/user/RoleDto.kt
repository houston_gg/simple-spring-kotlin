package tech.visdom.springappexample.dto.user

import org.apache.logging.log4j.util.Strings

data class RoleDto (
    var id: String = Strings.EMPTY,
    var description: String? = null
)
