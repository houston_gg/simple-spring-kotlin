package tech.visdom.springappexample.dto.user

import org.apache.logging.log4j.util.Strings

data class UserDto(
    var id: Long? = null,
    var firstName: String = Strings.EMPTY,
    var familyName: String = Strings.EMPTY,
    var username: String = Strings.EMPTY,
    var roles: Set<RoleDto> = mutableSetOf(),
)
