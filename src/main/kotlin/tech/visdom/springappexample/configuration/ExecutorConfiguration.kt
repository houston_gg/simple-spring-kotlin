package tech.visdom.springappexample.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler

@Configuration
class ExecutorConfiguration {

    @Bean
    @Primary
    fun defaultTaskScheduler() = ThreadPoolTaskScheduler().apply {
        poolSize = 5
        setThreadNamePrefix("default-task-scheduler-thread-pool-")
        initialize()
    }
}
