package tech.visdom.springappexample.configuration.jwt.token

enum class TokenType(
    type: String
) {
    ACCESS_TOKEN("ACCESS_TOKEN"),
    REFRESH_TOKEN("ACCESS_TOKEN"),
    UNKNOWN_TOKEN("UNKNOWN_TOKEN_TYPE");
}
