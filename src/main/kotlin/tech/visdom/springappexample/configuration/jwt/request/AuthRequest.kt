package tech.visdom.springappexample.configuration.jwt.request

import jakarta.validation.constraints.NotBlank

data class AuthRequest(
    @field:NotBlank val login: String?,
    @field:NotBlank val password: String?,
)
