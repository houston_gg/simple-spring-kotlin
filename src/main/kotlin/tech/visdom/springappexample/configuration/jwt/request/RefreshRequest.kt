package tech.visdom.springappexample.configuration.jwt.request

import jakarta.validation.constraints.NotEmpty

data class RefreshRequest(
    @field:NotEmpty val refreshToken: String?,
)
