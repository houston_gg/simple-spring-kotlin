package tech.visdom.springappexample.configuration.jwt

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.stereotype.Component
import tech.visdom.springappexample.configuration.jwt.token.TokenClaims
import tech.visdom.springappexample.configuration.jwt.token.TokenPair
import tech.visdom.springappexample.configuration.jwt.token.TokenType
import tech.visdom.springappexample.configuration.properties.JwtProperties
import tech.visdom.springappexample.entity.user.UserEntity
import java.util.*

@Component
class JwtProvider(
    private val jwtProperties: JwtProperties
) {

    fun generateTokenPair(user: UserEntity): TokenPair {
        with(jwtProperties) {
            val accessToken = generateToken(user, accessTokenExpirationTime, TokenType.ACCESS_TOKEN, accessSecret)
            val refreshToken = generateToken(user, refreshTokenExpirationTime, TokenType.REFRESH_TOKEN, refreshSecret)
            return TokenPair(accessToken, accessTokenExpirationTime, refreshToken, refreshTokenExpirationTime)
        }
    }

    fun validateToken(token: String, tokenType: TokenType): Boolean =
        runCatching { Jwts.parser().setSigningKey(getSecret(tokenType)).parseClaimsJws(token) }.isSuccess

    fun getTokenClaims(token: String, targetTokenType: TokenType): TokenClaims {
        val secret = getSecret(targetTokenType)
        val claims: Claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body

        return TokenClaims(
            claims.get("ID", String::class.java).toLong(),
            claims.get("SECURITY_STAMP", String::class.java),
            toTokenType(claims.get("TOKEN_TYPE", String::class.java))
        )
    }

    private fun generateToken(user: UserEntity, expirationTime: Long, tokenType: TokenType, secret: String): String {
        val expirationDate = Date(System.currentTimeMillis() + expirationTime)
        val claims = mapOf<String, Any>(
            "ID" to requireNotNull(user.id).toString(),
            "SECURITY_STAMP" to user.securityStamp,
            "TOKEN_TYPE" to tokenType
        )
        return Jwts.builder()
            .setClaims(claims)
            .setIssuer(jwtProperties.issuer)
            .setExpiration(expirationDate)
            .signWith(SignatureAlgorithm.HS512, secret)
            .setHeaderParam("typ", "JWT")
            .compact()
    }

    private fun toTokenType(extractedTokenType: String): TokenType = try {
        TokenType.valueOf(extractedTokenType)
    } catch (e: IllegalArgumentException) {
        TokenType.UNKNOWN_TOKEN
    }

    private fun getSecret(tokenType: TokenType): String = when(tokenType) {
        TokenType.ACCESS_TOKEN -> jwtProperties.accessSecret
        TokenType.REFRESH_TOKEN -> jwtProperties.refreshSecret
        else -> ""
    }
}
