package tech.visdom.springappexample.configuration.jwt

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import tech.visdom.springappexample.entity.user.UserEntity

class JwtUserDetails(
    user: UserEntity, authorities: MutableCollection<out GrantedAuthority>
) : UserDetails {

    private val username = user.username
    private val password = user.password
    private val isAccountNonLocked = !user.accountLocked
    private val securityStamp = user.securityStamp
    private val grantedAuthorities = authorities

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = grantedAuthorities

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = isAccountNonLocked

    fun getSecurityStamp(): String = securityStamp

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true

}
