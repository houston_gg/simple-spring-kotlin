package tech.visdom.springappexample.configuration.jwt.token

data class TokenPair(
    val accessToken: String,
    val accessTokenExpirationTime: Long,
    val refreshToken: String,
    val refreshTokenExpirationTime: Long,
)
