package tech.visdom.springappexample.configuration.jwt

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import tech.visdom.springappexample.configuration.jwt.token.TokenClaims
import tech.visdom.springappexample.configuration.jwt.token.TokenType
import tech.visdom.springappexample.service.user.UserDetailsServiceImpl

private const val AUTHORIZATION = "Authorization"

@Component
class JwtFilter(
    private val jwtProvider: JwtProvider,
    private val userDetailsService: UserDetailsServiceImpl
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        getTokenFromRequest(request)?.let { token ->
            if (jwtProvider.validateToken(token, TokenType.ACCESS_TOKEN)) {
                val accessTokenClaims = jwtProvider.getTokenClaims(token, TokenType.ACCESS_TOKEN)
                val userDetails: JwtUserDetails = userDetailsService.loadUserById(accessTokenClaims.id)
                if (checkUser(accessTokenClaims, userDetails)) {
                    SecurityContextHolder.getContext().authentication =
                        UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                }
            }
        }

        filterChain.doFilter(request, response)
    }

    private fun getTokenFromRequest(request: HttpServletRequest): String? {
        val bearerToken = request.getHeader(AUTHORIZATION)
        return if (!bearerToken.isNullOrBlank() && bearerToken.startsWith("Bearer ")) {
            bearerToken.substring(7)
        } else null
    }

    private fun checkUser(accessTokenClaims: TokenClaims, userDetails: JwtUserDetails): Boolean =
        accessTokenClaims.securityStamp == userDetails.getSecurityStamp() &&
            accessTokenClaims.tokenType == TokenType.ACCESS_TOKEN &&
            userDetails.isAccountNonLocked
}
