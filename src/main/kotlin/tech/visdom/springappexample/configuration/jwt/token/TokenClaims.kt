package tech.visdom.springappexample.configuration.jwt.token

data class TokenClaims(
    val id: Long,
    val securityStamp: String,
    val tokenType: TokenType,
)
