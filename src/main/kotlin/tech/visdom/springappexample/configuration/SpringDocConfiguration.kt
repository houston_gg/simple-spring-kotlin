package tech.visdom.springappexample.configuration

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springdoc.core.models.GroupedOpenApi
import org.springframework.boot.info.GitProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import tech.visdom.springappexample.SpringAppExampleApplication

private const val SECURITY_SCHEME_NAME = "bearerAuth"
private const val SCHEME = "bearer"
private const val FORMAT = "JWT"

@Configuration
class SpringDocConfiguration(
    private val gitProperties: GitProperties
) {

    @Bean
    fun defaultApi(): GroupedOpenApi = GroupedOpenApi.builder()
        .group("default")
        .packagesToScan(SpringAppExampleApplication::class.java.`package`.name)
        .build()

    @Bean
    fun authApi(): GroupedOpenApi = GroupedOpenApi.builder()
        .group("auth")
        .packagesToScan("tech.visdom.springappexample.controller.auth")
        .build()

    @Bean
    fun v1Api(): GroupedOpenApi = GroupedOpenApi.builder()
        .group("v1")
        .packagesToScan("tech.visdom.springappexample.controller.v1")
        .build()

    @Bean
    fun openAPI(): OpenAPI = with(gitProperties) {
        val securityItem = SecurityRequirement().addList(SECURITY_SCHEME_NAME)
        val securityScheme = SecurityScheme().apply {
            name = SECURITY_SCHEME_NAME
            type = SecurityScheme.Type.HTTP
            scheme = SCHEME
            bearerFormat = FORMAT
        }
        val components = Components().apply {
            addSecuritySchemes(SECURITY_SCHEME_NAME, securityScheme)
        }
        OpenAPI().info(
            Info()
                .title("SPRING APPLICATION EXAMPLE")
                .description("``${get("commit.id.abbrev")} | ${get("commit.message.full")}``")
                .version(get("closest.tag.name"))
        )
            .addSecurityItem(securityItem)
            .components(components)
    }
}
