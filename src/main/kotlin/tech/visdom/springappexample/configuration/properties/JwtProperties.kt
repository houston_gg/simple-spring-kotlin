package tech.visdom.springappexample.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "jwt")
class JwtProperties {
    var issuer: String = "VISDOM"
    lateinit var accessSecret: String
    lateinit var refreshSecret: String
    var accessTokenExpirationTime: Long = 3600000L
    var refreshTokenExpirationTime: Long = 7200000L
}
