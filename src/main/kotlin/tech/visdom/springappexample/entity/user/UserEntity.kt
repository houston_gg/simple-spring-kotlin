package tech.visdom.springappexample.entity.user

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.JoinTable
import jakarta.persistence.ManyToMany
import jakarta.persistence.Table
import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime

@Entity
@Table(name = "user")
class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "BIGSERIAL")
    var id: Long? = null

    @Column(name = "first_name", columnDefinition = "VARCHAR(256)", nullable = false)
    lateinit var firstName: String

    @Column(name = "family_name", columnDefinition = "VARCHAR(256)",  nullable = false)
    lateinit var familyName: String

    @Column(name = "username", columnDefinition = "VARCHAR(256)", unique = true,  nullable = false)
    lateinit var username: String

    @Column(name = "password", columnDefinition = "VARCHAR(256)", nullable = false)
    lateinit var password: String

    @Column(name = "security_stamp", columnDefinition = "VARCHAR(256)", nullable = false)
    lateinit var securityStamp: String

    @Column(name = "registration_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ", nullable = false)
    @CreationTimestamp
    lateinit var registrationDate: ZonedDateTime

    @Column(name = "last_password_change_date", columnDefinition = "TIMESTAMP")
    var lastPasswordChangeDate: ZonedDateTime? = null

    @Column(name = "last_password_reset_date", columnDefinition = "TIMESTAMP")
    var lastPasswordResetDate: ZonedDateTime? = null

    @Column(name = "account_locked", columnDefinition = "BOOLEAN", nullable = false)
    var accountLocked: Boolean = false

    @Column(name = "password_temporary", columnDefinition = "BOOLEAN", nullable = false)
    var passwordTemporary: Boolean = false

    @ManyToMany(fetch = FetchType.EAGER, cascade = [CascadeType.MERGE])
    @JoinTable(
        name = "user_role",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
    )
    var roles: MutableSet<RoleEntity> = mutableSetOf()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (familyName != other.familyName) return false
        if (username != other.username) return false
        if (password != other.password) return false
        if (securityStamp != other.securityStamp) return false
        if (registrationDate != other.registrationDate) return false
        if (lastPasswordChangeDate != other.lastPasswordChangeDate) return false
        if (lastPasswordResetDate != other.lastPasswordResetDate) return false
        if (accountLocked != other.accountLocked) return false
        if (passwordTemporary != other.passwordTemporary) return false
        if (roles != other.roles) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + firstName.hashCode()
        result = 31 * result + familyName.hashCode()
        result = 31 * result + username.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + securityStamp.hashCode()
        result = 31 * result + (registrationDate.hashCode())
        result = 31 * result + (lastPasswordChangeDate?.hashCode() ?: 0)
        result = 31 * result + (lastPasswordResetDate?.hashCode() ?: 0)
        result = 31 * result + accountLocked.hashCode()
        result = 31 * result + passwordTemporary.hashCode()
        result = 31 * result + roles.hashCode()
        return result
    }
}
