package tech.visdom.springappexample.entity.user

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "role")
class RoleEntity {

    @Id
    @Column(name = "id", columnDefinition = "VARCHAR(64)")
    lateinit var id: String

    @Column(name = "description", columnDefinition = "VARCHAR(256)")
    var description: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RoleEntity

        if (id != other.id) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (description?.hashCode() ?: 0)
        return result
    }
}
